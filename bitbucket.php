<?php

/**
 * Copyright © 2015 Jaroslav Hranička <hranicka@outlook.com>
 */

namespace Hranicka\BuildStatus;

require_once __DIR__ . '/libs/curl.php';
require_once __DIR__ . '/libs/rest.php';

// Build "auth" config
$auth = array_merge([
	'owner' => NULL,
	'repo_slug' => NULL,
	'revision' => NULL,
], $_GET);

if (isset($_GET['repository'])) {
	$repositoryInfo = explode('/', $_GET['repository']);
	$auth['owner'] = array_shift($repositoryInfo);
	$auth['repo_slug'] = array_shift($repositoryInfo);
}

// Build "post" config
$post = array_merge([
	'state' => NULL,
	'key' => NULL,
	'name' => NULL,
	'url' => NULL,
	'description' => NULL,
], $_GET);

// Log access
file_put_contents(__DIR__ . '/log/bitbucket.log', json_encode([
		'auth' => $auth,
		'post' => $post,
	]) . "\n", FILE_APPEND);


// REST
$rest = new RestClient(new CurlFactory([
	[CURLOPT_SSL_VERIFYPEER, FALSE],
]));

// OAuth
$ini = parse_ini_file(__DIR__ . '/config/oauth.ini', TRUE);
$rest->authorize('https://bitbucket.org/site/oauth2/access_token', array_merge(
	['grant_type' => 'client_credentials'],
	$ini[$auth['owner']]
));

// Update build status
$rest->post(
	"https://api.bitbucket.org/2.0/repositories/" .
	"{$auth['owner']}/{$auth['repo_slug']}/commit/{$auth['revision']}/statuses/build",
	$post
);
